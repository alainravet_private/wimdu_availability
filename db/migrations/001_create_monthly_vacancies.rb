Sequel.migration do
  change do
    create_table(:monthly_vacancies) do
      Fixnum :year
      Fixnum :month
      String :days_availabilities
    end
  end
end
