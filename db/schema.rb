Sequel.migration do
  change do
    create_table(:monthly_vacancies) do
      Integer :year
      Integer :month
      String :days_availabilities, :size=>255
    end
    
    create_table(:schema_info) do
      Integer :version, :default=>0, :null=>false
    end
  end
end
