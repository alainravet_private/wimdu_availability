# This is a helper class that unfolds the flat availablities ("00110011..")
# and lets us queries them via day numbers.

class MonthlyAvailabilitiesParser

  IS_AVAILABLE_VALUE = '0'

  class InvalidDayError < ArgumentError ; end

  def self.parse(days_availabilities)
    new(days_availabilities)
  end

  private_class_method :new
  def initialize(days_availabilities)
    @days_availabilities = days_availabilities
  end

  def available_on?(day_number)
    check_day_number_is_valid(day_number)
    day_index = day_number-1
    @days_availabilities[day_index] == IS_AVAILABLE_VALUE
  end


private

  def check_day_number_is_valid(day_number)
    first_day_number, last_day_number = 1 , @days_availabilities.length

    if day_number.to_i != day_number
      fail MonthlyAvailabilitiesParser::InvalidDayError,
           "invalid day number : #{day_number} (must be an integer between #{first_day_number} and #{last_day_number})"
    end

    if day_number < first_day_number || last_day_number < day_number
      fail MonthlyAvailabilitiesParser::InvalidDayError,
           "invalid day number : #{day_number} (must be between #{first_day_number} and #{last_day_number})"
    end
  end

end
