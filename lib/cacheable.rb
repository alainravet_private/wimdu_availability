module Cacheable
  def get_cached(key)
    cache[key] ||= yield
  end

  def reset_cache
    @cache = {}
  end

private

  def cache
    @cache ||= reset_cache
  end
end
