require_relative 'monthly_vacancies_store'
require_relative 'cacheable'
require_relative 'monthly_availabilities_parser'

class MonthlyVacancies
  include Cacheable

  def self.get
    new
  end

  def available_between?(checkin, checkout)
    dates = checkin..checkout-1   # we don't need to check for the checkout dates, as the client is leaving
    dates.all? { |date|
      available_on?(date)
    }
  ensure
    reset_cache # we bust the cache after each call, just to be safe. To optimize.
  end

private

  def available_on?(date)
    days_availabilities = days_availabilities_for(date)
    return false unless days_availabilities
    MonthlyAvailabilitiesParser.parse(days_availabilities).available_on?(date.day)
  end

  def days_availabilities_for(date)
    y, m           = date.year, date.month
    attributes = get_cached(cache_key = [y, m]) {
      MonthlyVacanciesStore.find_by_year_and_month(y, m)
    }
    attributes && attributes.fetch(:days_availabilities)
  end
end
