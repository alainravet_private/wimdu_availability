class MonthlyVacanciesStore
  def self.find_by_year_and_month(y, m)
    ::DB[:monthly_vacancies].where(:year => y, :month => m).first
  end
end
