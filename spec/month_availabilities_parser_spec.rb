require 'spec_helper'
require_relative '../lib/monthly_availabilities_parser'

describe MonthlyAvailabilitiesParser  do

  describe "available_on?" do

    it 'detects single available days' do
      MonthlyAvailabilitiesParser.parse('0011').available_on?(1).should eq(true)
      MonthlyAvailabilitiesParser.parse('0011').available_on?(2).should eq(true)
    end

    it 'detects single NON-available days' do
      MonthlyAvailabilitiesParser.parse('0011').available_on?(3).should eq(false)
      MonthlyAvailabilitiesParser.parse('0011').available_on?(4).should eq(false)
    end

    it 'raises an ArgumentError when the day number is invalid' do
      expect {
        MonthlyAvailabilitiesParser.parse('0011').available_on?(-1)
      }.to raise_error(MonthlyAvailabilitiesParser::InvalidDayError )

      last_day_number = 4
      expect {
        MonthlyAvailabilitiesParser.parse('0011').available_on?(last_day_number+1)
      }.to raise_error(MonthlyAvailabilitiesParser::InvalidDayError )
    end
  end

end
