require "spec_helper"

describe MonthlyVacancies do

  before do
                                                           #           1.........2.........31
                                                           #  1234567890123456789012345678901
    table = ::DB[:monthly_vacancies]
    table.insert(year: 2015, month:  1, days_availabilities: "1111111000000000000000000000000")
    table.insert(year: 2015, month:  6, days_availabilities: "000000000000000000001111111100")
    table.insert(year: 2015, month:  7, days_availabilities: "0000111111100000100000000000000")
    table.insert(year: 2015, month: 12, days_availabilities: "0000000000000000000111100000000")
  end

  subject(:monthly_vacancies) { MonthlyVacancies.get }

  describe "#available_between?" do
    it {
      is_expected.to be_available_between(Date.new(2015, 6, 29), Date.new(2015, 7, 5))
    }

    it { is_expected.to_not be_available_between(
                              Date.new(2015, 6, 28),
                              Date.new(2015, 7, 5)) }

    it { is_expected.to_not be_available_between(
                              Date.new(2015, 6, 29),
                              Date.new(2015, 7, 6)) }

    it do
      is_expected.to_not be_available_between(
                             Date.new(2015, 7, 15),
                             Date.new(2015, 7, 19))
    end

    it { is_expected.to_not be_available_between(     # NOTE: the original test assumed the contrary (? error)
                          Date.new(2015, 12, 24),     # , but it doesn't make much sense.
                          Date.new(2016, 1, 2)) }

    it { is_expected.to_not be_available_between(
                          Date.new(2015, 12, 22),
                          Date.new(2016, 1, 2)) }
  end
end

