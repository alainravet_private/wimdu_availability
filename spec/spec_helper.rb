require_relative "../lib/monthly_vacancies"

require 'sequel'
require 'sequel/extensions/migration'

module DbSetup
  def create_db(schema_location = "db/schema.rb")
    Sequel.sqlite.tap do |db|
      eval(
        File.read(File.expand_path("../../#{schema_location}", __FILE__)))
        .apply(db, :up)
    end
  end
end

::DB = Object.new.tap{|o| o.extend DbSetup}.create_db


RSpec.configure do |config|
  config.expect_with(:rspec) { |c| c.syntax = [:expect, :should] }
end
